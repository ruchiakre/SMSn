package NewGUI;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.*;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class Login 
{
	static WebDriver driver = new ChromeDriver();
	private static final String path= "/Achievers School[Ruchi]/Logindata.xlsx";
	//private static final String workbook = null;
	static String username;
	String password;
		public static void main(String[] args) throws IOException 
		
	{
		DriverInt();
	}
		
			public static void login(String u1, String p1 ) throws IOException 
				{
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				driver.findElement(By.id("email")).sendKeys(u1);
				driver.findElement(By.id("password")).sendKeys(p1);
	
				WebElement signin= driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[1]/form/button"));
				signin.click();
				
				driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
				
				String url= "http://uatn.theachieversschool.com/index";
				if(url.equals(driver.getCurrentUrl()))
				{
					System.out.println("Login: Successful Test Case: Passed");
					driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
					driver.findElement(By.xpath("/html/body/div[2]/header/div/div[2]/ul[2]/li[2]/a/div")).click();
					driver.findElement(By.linkText("Logout")).click();
					// FileOutputStream outputStream = new FileOutputStream(workbook);
					 
					  
	 
				}
				else
				{
					System.out.println("Login: Failed and Test Case: Passed");
				}
		
				
				}
			public static void readdata() throws IOException
				{
					try
					{
				
					System.setProperty("webdriver.chrome.driver","/SeleniumDriver/chromedriver");
					XSSFWorkbook workbook = new XSSFWorkbook(path);
					XSSFSheet datatypeSheet = workbook.getSheetAt(0);
					int totalNoOfRows = datatypeSheet.getLastRowNum();
					int total= datatypeSheet.getPhysicalNumberOfRows();
					System.out.println(totalNoOfRows);
					System.out.println(total);
					
					for(int row = 1; row <= totalNoOfRows; row++){
						
						XSSFRow rowdata = datatypeSheet.getRow(row);
						XSSFCell testcase= rowdata.getCell(1);
						//testcase.setCellValue("Pass");
						System.out.println("Test Statement: "+testcase);
						
						// get user Name
//						XSSFCell u1 = rowdata.getCell(2);
//						 CellType type = u1.getCellTypeEnum();
//						// System.out.println(type);
//						XSSFCell p1 = rowdata.getCell(3);
						// System.out.println(type);
						// System.out.println( p1.getCellTypeEnum());
						
						String username = rowdata.getCell(2).getStringCellValue();
						String password = rowdata.getCell(3).getStringCellValue();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						login(username,password);
}		
					
					driver.close();
								}
						catch(FileNotFoundException e)
							{
							throw new RuntimeException(e);
							}
					}

				
			public static void DriverInt() throws IOException 
			{
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				String appUrl = "http://uatn.theachieversschool.com/";
				
				// launch the fire fox browser and open the application URL
				driver.get(appUrl);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
				// maximize the browser window
				driver.manage().window().maximize();
				readdata();
			}
		
				}
	